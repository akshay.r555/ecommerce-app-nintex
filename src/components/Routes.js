import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from '../components/Home';
import paths from '../constants/Paths';

const App = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path={paths.ROOT} component={Home}/>
        </Switch>
    </BrowserRouter>
);

export default App;
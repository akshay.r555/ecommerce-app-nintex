export default {
    "ROOT": "/",
    "LOGIN": "/login",
    "REGISTER": "/register",
    "MAIN": "/main",
    "CHECKOUT": "/checkout" 
}
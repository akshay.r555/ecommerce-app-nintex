import { createStore } from 'redux';
// import thunk from 'redux-thunk';
import rootReducer from './Reducers';

const inititalState = {};

const store = createStore(
    rootReducer, 
    inititalState, 
    window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;